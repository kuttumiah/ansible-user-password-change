### Create `hosts` file
To run playbook you need to create a `inventory` file. Here is a sample
inventory file

    192.168.0.57    ansible_user=testuser

    [all:vars]
    new_password=v3ry3@$y


### Run playbook
To run this playbook run the command below

    $ ansible-playbook -i hosts password-changer.yml
